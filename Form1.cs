﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net; // NET USİNG EKLEMEMİZ GEREKİYOR
using System.Net.Mail;  // VE NET MAİL USİNG EKLEMEMİZ GEREKİYOR

namespace c_sharp_mail_gonderme
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SmtpClient sc = new SmtpClient(); // smtp sunucusu oluşturduk 
            sc.Port = 587;
            sc.Host = "smtp.gmail.com";
            sc.EnableSsl = true;
            // ve bilgilerini girdik gmail üzerinden işlem yapılacak
            try
            { // mail adresimizi yazarken örnek projevekod@gmail.com değilde projevekod olarak yazın.
                sc.Credentials = new NetworkCredential(gonderen.Text + "@gmail.com", sifre.Text);
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(gonderen.Text + "@gmail.com", konu.Text);
                mail.To.Add(alici.Text);
                mail.Subject = konu.Text;//konu
                mail.IsBodyHtml = true;
                mail.Body = icerik.Text;//icerik
                sc.Send(mail);
                MessageBox.Show("Mailiniz başarılı bir şekilde iletişmiştir.","Başarı");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bir hata gerçekleşti, hata ayrıntıları aşağıdadır.\n" + ex, "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
